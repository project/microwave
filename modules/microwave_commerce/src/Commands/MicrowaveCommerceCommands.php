<?php

namespace Drupal\microwave_commerce\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Url;
use Drupal\microwave\Services\MicrowaveBatchServiceInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush microwave commerce commands.
 */
class MicrowaveCommerceCommands extends DrushCommands {

  /**
   * The microwave config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $microwaveConfig;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The commerce product entity storage.
   *
   * @var \Drupal\commerce\CommerceContentEntityStorage
   */
  protected $productStorage;

  /**
   * Connection container.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce entity storage.
   *
   * @var \Drupal\commerce\CommerceContentEntityStorage
   */
  protected $commerceProductStorage;

  /**
   * Microwave batch service.
   *
   * @var \Drupal\microwave\Services\MicrowaveBatchServiceInterface
   */
  protected $microwaveBatchService;

  /**
   * Microwave commands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Database\Connection $connection
   *   Connection container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\microwave\Services\MicrowaveBatchServiceInterface $microwave_batch_service
   *   Microwave batch service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    QueueFactory $queue_factory,
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    MicrowaveBatchServiceInterface $microwave_batch_service,
  ) {
    $this->microwaveConfig = $config_factory->get('microwave.settings');
    $this->queueFactory = $queue_factory;
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->commerceProductStorage = $this->entityTypeManager->getStorage('commerce_product');
    $this->microwaveBatchService = $microwave_batch_service;
  }

  /**
   * Get product types from config, order them and set them in the queue.
   *
   * @command microwave:process_commerce_product_urls
   * @aliases mpcpu
   *
   * @usage microwave:process_commerce_product_urls
   */
  public function processCommerceProductUrls() {
    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->queueFactory->get('microwave_commerce_product_cron');
    if ($queue->numberOfItems() > 0) {
      $this->connection->query("DELETE FROM queue WHERE name='microwave_commerce_product_cron'");
      $this->logger()->notice(
        dt('The commerce product queue (microwave_commerce_product_cron) was not empty, previous items has been removed.')
      );
    }

    $selected_product_type = $this->microwaveConfig->get('selected_product_type') ?? [];
    $selected_product_type = array_values(array_filter($selected_product_type));
    if (empty($selected_product_type)) {
      $this->logger()->warning(
        dt('No product types configured for this process.')
      );
      return;
    }

    $operations = [];
    foreach ($selected_product_type as $bundle) {
      $days = (int) $this->microwaveConfig->get($bundle . '_days') ?? 0;
      $period_type = $this->microwaveConfig->get($bundle . '_period_type') ?? 0;
      $bundle_date_field = $this->microwaveConfig->get($bundle . '_date_field') ?? '';
      $query = $this->commerceProductStorage->getQuery();
      $this->microwaveBatchService->preBuildQuery(
        $query,
        'commerce_product',
        $bundle,
        $bundle_date_field,
        $days,
        $period_type
      );
      $pids = $query->execute();
      $max = count($pids);
      if ($max === 0) {
        $this->logger()->warning(
          dt('No commerce products @bundle were found with your configuration.', [
            '@bundle' => $bundle,
          ])
        );
        continue;
      }
      $operations[] = [
        [__CLASS__, 'processProductTypeWarming'],
        [$max, $bundle],
      ];
    }
    $batch = [
      'title' => dt('Queue ' . $bundle . ' URLs to warm.'),
      'operations' => $operations,
      'finished' => [__CLASS__, 'batchFinish'],
    ];

    batch_set($batch);
    drush_backend_batch_process();

  }

  /**
   * Callback to process product warming treating rows 100 by 100.
   *
   * @param int $max
   *   The maximum of item to process.
   * @param string $bundle
   *   The current bundle to process.
   * @param array|\ArrayAccess $context
   *   Standard batch context.
   */
  public static function processProductTypeWarming(int $max, string $bundle, &$context) {
    $microwave_config = \Drupal::config('microwave.settings');
    $bundle_date_field = $microwave_config->get($bundle . '_date_field') ?? '';
    $limit = 100;

    $days = (int) $microwave_config->get($bundle . '_days') ?? 0;
    $period_type = $microwave_config->get($bundle . '_period_type') ?? 0;
    $product_storage = \Drupal::entityTypeManager()->getStorage('commerce_product');
    $query = $product_storage->getQuery();
    $microwave_batch_service = \Drupal::service('microwave.batch_service');
    $microwave_batch_service->preBuildQuery(
      $query,
      'commerce_product',
      $bundle,
      $bundle_date_field,
      $days,
      $period_type
    );
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = $max;
      $context['sandbox']['last_loop'] = ceil($context['sandbox']['max'] / $limit);
    }
    $pids = $query
      ->condition('product_id', $context['sandbox']['current_id'], '>')
      ->sort('product_id', 'ASC')
      ->range(0, $limit)
      ->execute();

    $path_alias_manager = \Drupal::service('path_alias.manager');
    $queue_factory = \Drupal::service('queue');
    $products = $product_storage->loadMultiple($pids);
    foreach ($products as $product) {
      $alias = $path_alias_manager->getAliasByPath('/product/' . $product->id());
      $url = Url::fromUri(
        'internal:' . $alias,
        ['absolute' => TRUE]
      )->toString();
      /** @var \Drupal\Core\Queue\QueueInterface $queue */
      $queue = $queue_factory->get('microwave_commerce_product_cron');
      $item = new \stdClass();
      $item->url = $url;
      $queue->createItem($item);
      $context['sandbox']['current_id'] = $product->id();
      $context['results']['result'][] = $context['sandbox']['current_id'];
      $context['sandbox']['progress']++;
      $context['results']['bundle'][$bundle] = $context['sandbox']['progress'];
    }

    if ($context['sandbox']['progress'] !== $context['sandbox']['last_loop'] * $limit) {
      if ($context['sandbox']['progress'] !== $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      return;
    }
    $context['finished'] = 1;
  }

  /**
   * Just add a message when the process is finished.
   */
  public static function batchFinish($success, $results, $operations, $elapsed) {
    $message = t('Finished with an error.');
    $logger = \Drupal::logger('microwave_commerce');
    if (!$success || empty($results['result'])) {
      $logger->notice($message);
      return;
    }
    foreach ($results['bundle'] as $bundle => $total) {
      $message = \Drupal::translation()
        ->formatPlural(
          $total,
          $bundle . ': 1 product added to the queue.',
          $bundle . ': @count products added to the queue.'
        );

      $logger->notice($message);
    }
  }

}

<?php

namespace Drupal\microwave\Services;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class warmer requests.
 */
class WarmerRequests implements WarmerRequestsInterface {

  /**
   * The HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates a new WarmerRequests object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger service.
   */
  public function __construct(
    ClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_channel_factory,
  ) {
    $this->httpClient = $http_client;
    $this->logger = $logger_channel_factory->get('microwave');
  }

  /**
   * {@inheritdoc}
   */
  public function warmUrlByGet($data) {
    if (empty($data->url)) {
      return;
    }

    try {
      $response = $this->httpClient->request('GET', $data->url);
      $status_code = $response->getStatusCode();
      if ($status_code !== 200) {
        $this->logger->warning('MicrowaveQueueWorker failed to request %url : Response status code %status', [
          '%url' => $data->url,
          '%status' => $status_code,
        ]);
      }
    }
    catch (\Exception $e) {
      $this->logger->warning('%message', [
        '%message' => $e->getMessage(),
      ]);
    }
  }

}

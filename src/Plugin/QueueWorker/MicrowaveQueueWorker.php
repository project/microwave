<?php

namespace Drupal\microwave\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Microwave Queue Worker.
 *
 * @QueueWorker(
 *   id = "cron_content_warmer",
 *   title = @Translation("Cron Content Warmer"),
 *   cron = {"time" = 10}
 * )
 */
class MicrowaveQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates a new MicrowaveQueueWorker object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger service.
   */
  public function __construct(
    ClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_channel_factory,
  ) {
    $this->httpClient = $http_client;
    $this->logger = $logger_channel_factory->get('microwave');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $container->get('http_client'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data->url)) {
      return;
    }

    try {
      $response = $this->httpClient->request('GET', $data->url);
      $status_code = $response->getStatusCode();
      if ($status_code !== 200) {
        $this->logger->warning('MicrowaveQueueWorker failed to request %url : Response status code %status', [
          '%url' => $data->url,
          '%status' => $status_code,
        ]);
      }
    }
    catch (\Exception $e) {
      $this->logger->warning('%message', [
        '%message' => $e->getMessage(),
      ]);
    }
  }

}

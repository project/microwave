CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Additional module
 * Configuration
 * Drush commands
 * Drupal command
 * Similar projects and how they are different
 * Maintainers


INTRODUCTION
------------

**Microwave** helps you to warm your website pages (node/taxonomy term/custom
pages/commerce product). It is sometimes useful to generate page cache after a
release delivery on production in order to prevent heavy loading for a real user
(human).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/microwave

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/microwave


REQUIREMENTS
------------

- Drush 10.x
`composer require drush/drush`
- Drupal console 1.9.x
`composer require drupal/console`

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


additional MODULE
------------------

- Microwave commerce:
Include commerce product page warming. Same process as nodes or vocabularies,
it allows you to select product bundle and additional parameters in order to
warm product page.


CONFIGURATION
-------------

Go to `/admin/config/system/microwave` and configure which entities and/or
custom urls you want to warm.


DRUSH COMMANDS
--------------

After you filled the settings form, you can use the following commands to add
the urls to the warmer queue.

- Custom urls:
`drush microwave:process_custom_urls` or its alias `drush mpcu`

- Nodes:
`drush microwave:process_nodes_urls` or its alias `drush mpnu`

- Taxonomy terms:
`drush microwave:process_terms_urls` or its alias `drush mptu`

- Commerce product:
`drush microwave:process_commerce_product_urls` or its alias `drush mpcpu`

Those commands can be called from a CI.

DRUPAL COMMAND
--------------

You can call those commands if you want to warm your pages right now or add it
to the crontab:
- Custom urls:
`drupal queue:run microwave_custom_cron` or its alias
`drupal qr microwave_custom_cron`
- Nodes:
`drupal queue:run microwave_node_cron` or its alias
`drupal qr microwave_node_cron`
- Taxonomy terms:
`drupal queue:run microwave_term_cron` or its alias
`drupal qr microwave_term_cron`
- Commerce product:
`drupal queue:run microwave_commerce_product_cron` or its alias
`drupal qr microwave_commerce_product_cron`


SIMILAR PROJECTS AND HOW THEY ARE DIFFERENT
-------------------------------------------

[Warmer](https://www.drupal.org/project/warmer) is slightly different since
microwave is a bit more granular: you can choose not to warm every nodes of a
content type by setting a date field and a period.


MAINTAINERS
-----------

Current maintainers:
 * Thibault Maquin (t.maquin) - https://www.drupal.org/u/tmaquin-0
 * Maxime Roux (MacSim) - https://www.drupal.org/u/macsim
 * Anthony Delgado (adelgado12) - https://www.drupal.org/u/adelgado12
 * Axel Depret (a.depret) - https://www.drupal.org/u/adepret

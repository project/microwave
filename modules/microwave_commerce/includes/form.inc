<?php

/**
 * @file
 * Microwave commerce form hooks.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\microwave_commerce\Alter\Form\AdminMicrowaveForm;

/**
 * Implements hook_form_alter().
 */
function microwave_commerce_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === "microwave_settings") {
    \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(AdminMicrowaveForm::class)
      ->alterForm($form, $form_state);
  }
}

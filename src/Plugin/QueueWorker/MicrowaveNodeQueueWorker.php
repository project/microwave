<?php

namespace Drupal\microwave\Plugin\QueueWorker;

/**
 * Microwave node queue worker.
 *
 * @QueueWorker(
 *   id = "microwave_node_cron",
 *   title = @Translation("Microwave node cron"),
 *   cron = {"time" = 10}
 * )
 */
class MicrowaveNodeQueueWorker extends MicrowaveBaseQueueWorker {}

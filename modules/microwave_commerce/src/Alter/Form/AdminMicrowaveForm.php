<?php

namespace Drupal\microwave_commerce\Alter\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Admin microwave settings form alter.
 */
final class AdminMicrowaveForm implements ContainerInjectionInterface {

  const DATE_FIELD_TYPES = [
    'created',
    'changed',
    'timestamp',
    'datetime',
    'daterange',
  ];

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * Microwave configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $microwaveConfig;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity bundle info.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_bundle_info,
    ConfigFactoryInterface $config_factory,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
    $this->microwaveConfig = $config_factory->get('microwave.settings');
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * Alter the form.
   *
   * @param array $form
   *   The form rendering array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The actual form state.
   */
  public function alterForm(array &$form, FormStateInterface $form_state) {
    $bundles_info = $this->entityBundleInfo->getBundleInfo('commerce_product');
    $options = [];
    if (!empty($bundles_info)) {
      foreach ($bundles_info as $bundle_name => $bundle_info) {
        $options[$bundle_name] = $bundle_info['label'];
      }
    }

    if (!empty($options)) {
      $commerce_product['commerce_product'] = [
        '#type' => 'details',
        '#title' => $this->t('Product'),
        '#open' => FALSE,
      ];
      $form = array_merge(array_slice($form, 0, 3), $commerce_product, array_slice($form, 3));

      $this->buildSelectCommerceProductField($form, $options);
      $this->buildSelectedParams($form, $options);
    }
  }

  /**
   * Build the select commerce product field element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $options
   *   An array of product types.
   */
  protected function buildSelectCommerceProductField(array &$form, array $options) {
    $selected_product_types = $this->microwaveConfig->get('selected_product_type') ?? [];
    if (!empty($selected_product_types)) {
      $selected_product_types = array_values(array_filter($selected_product_types));
    }
    $form['commerce_product']['selected_product_type'] = [
      '#type' => 'checkboxes',
      '#default_value' => $selected_product_types,
      '#options' => $options,
    ];
  }

  /**
   * Build the selected params element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $options
   *   An array of commerce product types.
   */
  protected function buildSelectedParams(array &$form, array $options) {
    foreach ($options as $bundle => $product_type_label) {
      $form['commerce_product'][$bundle]['selected_parameters'] = [
        '#type' => 'details',
        '#title' => $this->t(':content_type - additional params', [
          ':content_type' => $product_type_label,
        ]),
        '#open' => FALSE,
        '#description' => $this->t('Those params make the list of products to warm more granular.<br/>%product_type nodes to warm can be filtered based on a date field and a period.', [
          '%product_type' => $product_type_label,
        ]),
        '#states' => [
          'visible' => [
            ':input[name="selected_product_type[' . $bundle . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $field_definitions = $this->entityFieldManager->getFieldDefinitions('commerce_product', $bundle);
      $options = [
        '_none' => $this->t('None'),
      ];
      foreach ($field_definitions as $field_name => $field_definition) {
        if (!in_array($field_definition->getType(), self::DATE_FIELD_TYPES)) {
          continue;
        }
        $options[$field_name] = $field_definition->getLabel() . ' (' . $field_name . ')';
        if ($field_definition->getType() === "daterange") {
          $options[$field_name] .= ', ' . $this->t('Start date');
        }
      }

      $form['commerce_product'][$bundle]['selected_parameters'][$bundle . '_date_field'] = [
        '#type' => 'radios',
        '#title' => $this->t('Filter by date field'),
        '#options' => $options,
        '#default_value' => $this->microwaveConfig->get($bundle . '_date_field') ?? '_none',
      ];

      $form['commerce_product'][$bundle]['selected_parameters']['period'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Period'),
        '#description' => $this->t('This time interval filters the content to warm.'),
        '#states' => [
          'visible' => [
            ':input[name="' . $bundle . '_date_field"]' => ['!value' => '_none'],
          ],
        ],
        '#required' => TRUE,
      ];

      $form['commerce_product'][$bundle]['selected_parameters']['period'][$bundle . '_days'] = [
        '#type' => 'number',
        '#title' => $this->t('Number of days'),
        '#field_suffix' => $this->t('days'),
        '#min' => 0,
        '#default_value' => $this->microwaveConfig->get($bundle . '_days') ?? NULL,
      ];

      $options = [
        'minus' => $this->t('-X days from today'),
        'more' => $this->t('+X days from today'),
        'minus_more' => $this->t('From -X days to +X days from today'),
      ];
      $form['commerce_product'][$bundle]['selected_parameters']['period'][$bundle . '_period_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Period type'),
        '#options' => $options,
        '#default_value' => $this->microwaveConfig->get($bundle . '_period_type') ?? NULL,
      ];
    }
  }

}

<?php

namespace Drupal\microwave\Plugin\QueueWorker;

/**
 * Microwave custom queue worker.
 *
 * @QueueWorker(
 *   id = "microwave_custom_cron",
 *   title = @Translation("Microwave custom cron"),
 *   cron = {"time" = 10}
 * )
 */
class MicrowaveCustomQueueWorker extends MicrowaveBaseQueueWorker {}

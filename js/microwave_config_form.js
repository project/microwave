(function ($, Drupal) {
  Drupal.behaviors.microwaveConfigForm = {

    /**
     * @param  {HTMLElement} context
     */
    attach: function (context) {
      $('#edit-urls', context).once('microwaveConfigForm').bind('change keyup', function (event) {
        var value = '';
        var splitValue = $(this).val().split("\n");

        for (var a = 0; a <= 200 && typeof splitValue[a] != 'undefined'; a++) {
          if (a > 0) value += "\n";
          value += splitValue[a];
        }
        $(this).val(value);
      });
    }
  }
})(jQuery, Drupal)

<?php

namespace Drupal\microwave_commerce\Plugin\QueueWorker;

use Drupal\microwave\Plugin\QueueWorker\MicrowaveBaseQueueWorker;

/**
 * Microwave commerce product queue worker.
 *
 * @QueueWorker(
 *   id = "microwave_commerce_product_cron",
 *   title = @Translation("Microwave commerce product cron"),
 *   cron = {"time" = 10}
 * )
 */
class MicrowaveCommerceProductQueueWorker extends MicrowaveBaseQueueWorker {}

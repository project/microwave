<?php

namespace Drupal\microwave\Services;

use Drupal\Core\Entity\Query\Sql\Query;

/**
 * Interface microwave batch service.
 */
interface MicrowaveBatchServiceInterface {

  /**
   * Prebuild query for content type command.
   *
   * @param \Drupal\Core\Entity\Query\Sql\Query $query
   *   The query.
   * @param string $entity_type
   *   The entity type to query.
   * @param string $bundle
   *   The bundle to search.
   * @param string $bundle_date_field
   *   The name of the date field.
   * @param int $days
   *   Number of days to filter.
   * @param string $period_type
   *   The period type.
   */
  public function preBuildQuery(
    Query &$query,
    string $entity_type,
    string $bundle,
    string $bundle_date_field,
    int $days,
    string $period_type,
  );

}

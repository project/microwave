<?php

namespace Drupal\microwave\Services;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\Query;

/**
 * Class microwave batch service.
 */
class MicrowaveBatchService implements MicrowaveBatchServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Microwave batch service constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function preBuildQuery(
    Query &$query,
    string $entity_type,
    string $bundle,
    string $bundle_date_field,
    int $days,
    string $period_type,
  ) {
    $query->condition('type', $bundle)
      ->condition('status', 1);
    if (
      !empty($bundle_date_field) &&
      '_none' !== $bundle_date_field &&
      !empty($days) &&
      !empty($period_type)
    ) {
      $start_date = $period_type === 'more' ? time() : strtotime('-' . $days . ' days', time());
      $end_date = $period_type === 'minus' ? time() : strtotime('+' . $days . ' days', time());

      $fields_infos = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
      $field_type = $fields_infos[$bundle_date_field]->getType();
      if (in_array($field_type, ['datetime', 'daterange'])) {
        $start_date = date('Y-m-d H:i:s', $start_date);
        $start_date = new DrupalDateTime($start_date, new \DateTimeZone('Europe/Paris'));
        $end_date = date('Y-m-d H:i:s', $end_date);
        $end_date = new DrupalDateTime($end_date, new \DateTimeZone('Europe/Paris'));
      }

      $query->condition($bundle_date_field, $start_date, '>=')
        ->condition($bundle_date_field, $end_date, '<=');
    }
  }

}

<?php

namespace Drupal\microwave\Services;

/**
 * Interface warmer requests.
 */
interface WarmerRequestsInterface {

  /**
   * Warm url by GET request.
   *
   * @param mixed $data
   *   The data that was passed to
   *   \Drupal\Core\Queue\QueueInterface::createItem() when the item was queued.
   */
  public function warmUrlByGet($data);

}

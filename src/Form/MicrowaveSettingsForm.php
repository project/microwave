<?php

namespace Drupal\microwave\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Microwave settings form.
 */
final class MicrowaveSettingsForm extends ConfigFormBase {

  const DATE_FIELD_TYPES = [
    'created',
    'changed',
    'timestamp',
    'datetime',
    'daterange',
  ];

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * The current host.
   *
   * @var string
   */
  protected $host;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path alias storage.
   *
   * @var \Drupal\path_alias\PathAliasStorage
   */
  protected $pathAliasStorage;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The microwave config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $microwaveConfig;

  /**
   * Constructs a \Drupal\microwave\Form\MicrowaveSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity bundle info.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    EntityTypeBundleInfoInterface $entity_bundle_info,
    RequestStack $request_stack,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->microwaveConfig = $this->config($this->getConfigName());
    $this->entityBundleInfo = $entity_bundle_info;
    $this->host = $request_stack->getCurrentRequest()->getSchemeAndHttpHost();
    $this->entityTypeManager = $entity_type_manager;
    $this->pathAliasStorage = $this->entityTypeManager->getStorage('path_alias');
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.bundle.info'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName() {
    return 'microwave.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->getConfigName()];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'microwave_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    $this->buildContentTypeField($form);
    $this->buildVocabulariesField($form);
    $this->buildCustomUrls($form);

    $form['#attached']['library'][] = 'microwave/microwave_config_form';

    return parent::buildForm($form, $form_state);
  }

  /**
   * Build the vocabularies element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  protected function buildVocabulariesField(array &$form) {
    $vocab_storage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    $vocabularies = $vocab_storage->loadMultiple();

    $options = [];
    if (!empty($vocabularies)) {
      foreach ($vocabularies as $voc_id => $voc) {
        $options[$voc_id] = $voc->label();
      }

      $selected_vocabularies = $this->microwaveConfig->get('selected_vocabularies') ?? [];
      $form['vocabularies'] = [
        '#type' => 'details',
        '#title' => $this->t('Vocabularies'),
        '#open' => !empty($selected_vocabularies),
      ];

      $this->buildSelectVocabulariesField($form, $options, $selected_vocabularies);
    }
  }

  /**
   * Build the select vocabularies element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $options
   *   An array of available vocabularies.
   * @param array $selected_vocabularies
   *   An array of selected vocabularies.
   */
  protected function buildSelectVocabulariesField(array &$form, array $options, array $selected_vocabularies) {
    if (!empty($selected_vocabularies)) {
      $selected_vocabularies = array_values(array_filter($selected_vocabularies));
    }
    $form['vocabularies']['selected_vocabularies'] = [
      '#type' => 'checkboxes',
      '#default_value' => $selected_vocabularies,
      '#options' => $options,
      '#description' => $this->t('Note: only the first page of the taxonomy terms will be warmed.'),
    ];
  }

  /**
   * Build the content type element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  protected function buildContentTypeField(array &$form) {
    $bundles_info = $this->entityBundleInfo->getBundleInfo('node');
    $options = [];
    if (!empty($bundles_info)) {
      foreach ($bundles_info as $bundle_name => $bundle_info) {
        $options[$bundle_name] = $bundle_info['label'];
      }
    }

    $form['content_types'] = [
      '#type' => 'details',
      '#title' => $this->t('Content types'),
      '#open' => TRUE,
    ];

    $this->buildSelectContentTypeField($form, $options);
    $this->buildSelectedParams($form, $options);
  }

  /**
   * Build the select content type element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $options
   *   An array of content types.
   */
  protected function buildSelectContentTypeField(array &$form, array $options) {
    $selected_content_types = $this->microwaveConfig->get('selected_content_type') ?? [];
    if (!empty($selected_content_types)) {
      $selected_content_types = array_values(array_filter($selected_content_types));
    }
    $form['content_types']['selected_content_type'] = [
      '#type' => 'checkboxes',
      '#default_value' => $selected_content_types,
      '#options' => $options,
    ];
  }

  /**
   * Build the selected params element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $options
   *   An array of content types.
   */
  protected function buildSelectedParams(array &$form, array $options) {
    foreach ($options as $bundle => $content_type_label) {
      $form['content_types'][$bundle]['selected_parameters'] = [
        '#type' => 'details',
        '#title' => $this->t(':content_type - additional params', [
          ':content_type' => $content_type_label,
        ]),
        '#open' => FALSE,
        '#description' => $this->t('Those params make the list of contents to warm more granular.<br/>%content_type nodes to warm can be filtered based on a date field and a period.', [
          '%content_type' => $content_type_label,
        ]),
        '#states' => [
          'visible' => [
            ':input[name="selected_content_type[' . $bundle . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $bundle);
      $options = [
        '_none' => $this->t('None'),
      ];
      foreach ($field_definitions as $field_name => $field_definition) {
        if (!in_array($field_definition->getType(), self::DATE_FIELD_TYPES)) {
          continue;
        }
        $options[$field_name] = $field_definition->getLabel() . ' (' . $field_name . ')';
        if ($field_definition->getType() === "daterange") {
          $options[$field_name] .= ', ' . $this->t('Start date');
        }
      }

      $form['content_types'][$bundle]['selected_parameters'][$bundle . '_date_field'] = [
        '#type' => 'radios',
        '#title' => $this->t('Filter by date field'),
        '#options' => $options,
        '#default_value' => $this->microwaveConfig->get($bundle . '_date_field') ?? '_none',
      ];

      $form['content_types'][$bundle]['selected_parameters']['period'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Period'),
        '#description' => $this->t('This time interval filters the content to warm.'),
        '#states' => [
          'visible' => [
            ':input[name="' . $bundle . '_date_field"]' => ['!value' => '_none'],
          ],
        ],
        '#required' => TRUE,
      ];

      $form['content_types'][$bundle]['selected_parameters']['period'][$bundle . '_days'] = [
        '#type' => 'number',
        '#title' => $this->t('Number of days'),
        '#field_suffix' => $this->t('days'),
        '#min' => 0,
        '#default_value' => $this->microwaveConfig->get($bundle . '_days') ?? NULL,
      ];

      $options = [
        'minus' => $this->t('-X days from today'),
        'more' => $this->t('+X days from today'),
        'minus_more' => $this->t('From -X days to +X days from today'),
      ];
      $form['content_types'][$bundle]['selected_parameters']['period'][$bundle . '_period_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Period type'),
        '#options' => $options,
        '#default_value' => $this->microwaveConfig->get($bundle . '_period_type') ?? NULL,
      ];
    }
  }

  /**
   * Build the urls list element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  protected function buildCustomUrls(array &$form) {
    $form['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Specific URLs'),
      '#description' => $this->t('Non-entity pages that need to be warmed.<br/>One URL per line (200 lines max).'),
      '#default_value' => $this->microwaveConfig->get('urls') ?? '',
      '#rows' => 30,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $val = $form_state->getValues();
    $selected_content_types = array_values(array_filter($val['selected_content_type']));

    foreach ($selected_content_types as $bundle) {
      if ('_none' === $val[$bundle . '_date_field']) {
        continue;
      }
      if (empty($val[$bundle . '_days'])) {
        $form_state->setErrorByName($bundle . '_days', $this->t('The period is not set for the content type %content_type', [
          '%content_type' => $bundle,
        ]));
      }
      if (empty($val[$bundle . '_period_type'])) {
        $form_state->setErrorByName($bundle . '_period_type', $this->t('The period type is not set for the content type %content_type', [
          '%content_type' => $bundle,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config($this->getConfigName());

    $form_state
      ->addCleanValueKey('container')
      ->addCleanValueKey('actions');

    $values = $form_state->cleanValues()->getValues();
    $values['updated'] = time();

    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }

    $content_type_options = array_keys($form['content_types']['selected_content_type']['#options']);
    $selected_content_types = $values['selected_content_type'] ?? [];
    foreach ($content_type_options as $bundle) {
      if (in_array($bundle, $selected_content_types)) {
        continue;
      }
      $config->clear($bundle . '_date_field');
      $config->clear($bundle . '_days');
      $config->clear($bundle . '_period_type');
    }

    $config->save();

    Cache::invalidateTags([$this->getConfigName()]);
  }

}

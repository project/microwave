<?php

namespace Drupal\microwave\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\microwave\Services\WarmerRequestsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the microwave queue workers.
 */
class MicrowaveBaseQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The warmer requests service.
   *
   * @var \Drupal\microwave\Services\WarmerRequestsInterface
   */
  protected $warmerRequests;

  /**
   * Creates a new MicrowaveQueueWorker object.
   *
   * @param \Drupal\microwave\Services\WarmerRequestsInterface $warmer_requests
   *   The warmer requests service.
   */
  public function __construct(
    WarmerRequestsInterface $warmer_requests,
  ) {
    $this->warmerRequests = $warmer_requests;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $container->get('microwave.warmer_requests')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->warmerRequests->warmUrlByGet($data);
  }

}

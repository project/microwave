<?php

namespace Drupal\microwave\Plugin\QueueWorker;

/**
 * Microwave term queue worker.
 *
 * @QueueWorker(
 *   id = "microwave_term_cron",
 *   title = @Translation("Microwave term cron"),
 *   cron = {"time" = 10}
 * )
 */
class MicrowaveTermQueueWorker extends MicrowaveBaseQueueWorker {}

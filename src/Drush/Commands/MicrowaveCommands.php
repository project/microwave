<?php

namespace Drupal\microwave\Drush\Commands;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Url;
use Drupal\microwave\Services\MicrowaveBatchServiceInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Drush microwave commands.
 */
class MicrowaveCommands extends DrushCommands {

  /**
   * The microwave config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $microwaveConfig;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The current host.
   *
   * @var string
   */
  protected $host;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * The Term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  protected $termStorage;

  /**
   * Connection container.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Microwave batch service.
   *
   * @var \Drupal\microwave\Services\MicrowaveBatchServiceInterface
   */
  protected $microwaveBatchService;

  /**
   * Microwave commands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   Connection container.
   * @param \Drupal\microwave\Services\MicrowaveBatchServiceInterface $microwave_batch_service
   *   Microwave batch service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    QueueFactory $queue_factory,
    RequestStack $request_stack,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    MicrowaveBatchServiceInterface $microwave_batch_service,
  ) {
    $this->microwaveConfig = $config_factory->get('microwave.settings');
    $this->queueFactory = $queue_factory;
    $this->host = $request_stack->getCurrentRequest()->getSchemeAndHttpHost();
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->connection = $connection;
    $this->microwaveBatchService = $microwave_batch_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('queue'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('microwave.batch_service'),
    );
  }

  /**
   * Get custom urls from config and set them in the queue.
   *
   * @command microwave:process_custom_urls
   * @aliases mpcu
   *
   * @usage microwave:process_custom_urls
   */
  public function processCustomUrls() {
    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->queueFactory->get('microwave_custom_cron');
    if ($queue->numberOfItems() > 0) {
      $this->connection->query("DELETE FROM queue WHERE name='microwave_custom_cron'");
      $this->logger()->notice(
        dt('The custom url queue (microwave_custom_cron) was not empty and has been cleared.')
      );
    }

    $urls = $this->microwaveConfig->get('urls');
    if (empty($urls)) {
      $this->logger()->warning(
        dt('There are no URLs to queue.')
      );
      return;
    }

    $urls = explode(PHP_EOL, $urls);
    foreach ($urls as $url) {
      $url = str_replace(
        ["\r\n", "\n", "\r"],
        '',
        $url
      );
      $item = new \stdClass();
      $item->url = $url;
      $queue->createItem($item);
    }

    $this->logger()->notice(
      dt('All urls have been queued.')
    );

  }

  /**
   * Get vocabularies from config and add their terms URLs in the queue.
   *
   * @command microwave:process_terms_urls
   * @aliases mptu
   *
   * @usage microwave:process_terms_urls
   */
  public function processTermsUrls() {
    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->queueFactory->get('microwave_term_cron');
    if ($queue->numberOfItems() > 0) {
      $this->connection->query("DELETE FROM queue WHERE name='microwave_term_cron'");
      $this->logger()->notice(
        dt('The vocabulary queue (microwave_term_cron) was not empty and has been cleared.')
      );
    }
    $selected_vocabularies = $this->microwaveConfig
      ->get('selected_vocabularies') ?? [];
    $selected_vocabularies = array_values(
      array_filter($selected_vocabularies)
    );
    if (empty($selected_vocabularies)) {
      $this->logger()->warning(
        dt('No vocabularies configured for this process.')
      );
      return;
    }

    $operations = [];
    foreach ($selected_vocabularies as $vocabulary) {
      $query = $this->termStorage->getQuery()
        ->accessCheck(TRUE)
        ->condition('vid', $vocabulary)
        ->condition('status', 1);
      $tids = $query->execute();
      $max = count($tids);
      if ($max === 0) {
        $this->logger()->warning(
          dt('No terms for @vocabulary vocabulary were found with your configuration.', [
            '@vocabulary' => $vocabulary,
          ])
        );
        continue;
      }
      $operations[] = [
        [__CLASS__, 'processVocabulariesWarming'],
        [$max, $vocabulary],
      ];
    }

    if (empty($operations)) {
      return;
    }

    $batch = [
      'title' => dt('Queue ' . $vocabulary . ' URLs to warm.'),
      'operations' => $operations,
      'finished' => [__CLASS__, 'batchFinish'],
    ];

    batch_set($batch);
    drush_backend_batch_process();
  }

  /**
   * Callback to process terms warming treating rows 100 by 100.
   *
   * @param int $max
   *   The maximum of item to process.
   * @param string $vocabulary
   *   The vocabulary to process.
   * @param array|\ArrayAccess $context
   *   Standard batch context.
   */
  public static function processVocabulariesWarming(int $max, string $vocabulary, &$context) {
    $limit = 100;
    $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $query = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('vid', $vocabulary)
      ->condition('status', 1);

    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = $max;
      $context['sandbox']['last_loop'] = $limit <= $context['sandbox']['max']
        ? ceil($context['sandbox']['max'] / $limit)
        : 1;
    }

    $tids = $query
      ->condition('tid', $context['sandbox']['current_id'], '>')
      ->sort('tid', 'ASC')
      ->range(0, $limit)
      ->execute();

    $queue_factory = \Drupal::service('queue');
    $terms = $storage->loadMultiple($tids);
    foreach ($terms as $term) {
      $url = Url::fromRoute('entity.taxonomy_term.canonical', [
        'taxonomy_term' => $term->id(),
        'absolute' => TRUE,
      ])->toString();
      /** @var \Drupal\Core\Queue\QueueInterface $queue */
      $queue = $queue_factory->get('microwave_term_cron');
      $item = new \stdClass();
      $item->url = $url;
      $queue->createItem($item);
      $context['sandbox']['current_id'] = $term->id();
      $context['results']['result'][] = $context['sandbox']['current_id'];
      $context['sandbox']['progress']++;
      $context['results']['bundle'][$vocabulary] = $context['sandbox']['progress'];
    }

    if ($context['sandbox']['progress'] !== $context['sandbox']['last_loop'] * $limit) {
      if ($context['sandbox']['progress'] !== $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      return;
    }

    $context['finished'] = 1;
  }

  /**
   * Get content types from config, order them and set them in the queue.
   *
   * @command microwave:process_nodes_urls
   * @aliases mpnu
   *
   * @usage microwave:process_nodes_urls
   */
  public function processNodesUrls() {
    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->queueFactory->get('microwave_node_cron');
    if ($queue->numberOfItems() > 0) {
      $this->connection->query("DELETE FROM queue WHERE name='microwave_node_cron'");
      $this->logger()->notice(
        dt('The node queue (microwave_node_cron) was not empty, previous items has been removed.')
      );
    }

    $selected_content_type = $this->microwaveConfig->get('selected_content_type') ?? [];
    $selected_content_type = array_values(array_filter($selected_content_type));
    if (empty($selected_content_type)) {
      $this->logger()->warning(
        dt('No content types configured for this process.')
      );
      return;
    }

    $operations = [];
    foreach ($selected_content_type as $bundle) {
      $bundle_date_field = $this->microwaveConfig->get($bundle . '_date_field') ?? '';
      $days = (int) $this->microwaveConfig->get($bundle . '_days') ?? 0;
      $period_type = $this->microwaveConfig->get($bundle . '_period_type') ?? 0;
      $query = $this->nodeStorage->getQuery();
      $this->microwaveBatchService->preBuildQuery(
        $query,
        'node',
        $bundle,
        $bundle_date_field,
        $days,
        $period_type
      );
      $nids = $query->execute();
      $max = count($nids);
      if ($max === 0) {
        $this->logger()->warning(
          dt('No nodes @bundle were found with your configuration.', [
            '@bundle' => $bundle,
          ])
        );
        continue;
      }
      $operations[] = [
        [__CLASS__, 'processContentTypeWarming'],
        [$max, $bundle],
      ];
    }

    if (empty($operations)) {
      return;
    }

    $batch = [
      'title' => dt('Queue ' . $bundle . ' URLs to warm.'),
      'operations' => $operations,
      'finished' => [__CLASS__, 'batchFinish'],
    ];

    batch_set($batch);
    drush_backend_batch_process();

  }

  /**
   * Callback to process content type warming treating rows 100 by 100.
   *
   * @param int $max
   *   The maximum of item to process.
   * @param string $bundle
   *   The current bundle to process.
   * @param array|\ArrayAccess $context
   *   Standard batch context.
   */
  public static function processContentTypeWarming(int $max, string $bundle, &$context) {
    $microwave_config = \Drupal::config('microwave.settings');
    $bundle_date_field = $microwave_config->get($bundle . '_date_field') ?? '';
    $limit = 100;

    $days = (int) $microwave_config->get($bundle . '_days') ?? 0;
    $period_type = $microwave_config->get($bundle . '_period_type') ?? 0;
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $query = $storage->getQuery();
    $microwave_batch_service = \Drupal::service('microwave.batch_service');
    $microwave_batch_service->preBuildQuery(
      $query,
      'node',
      $bundle,
      $bundle_date_field,
      $days,
      $period_type
    );
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $nids = $query->execute();
      $context['sandbox']['max'] = $max;
      $context['sandbox']['last_loop'] = ceil($context['sandbox']['max'] / $limit);
    }
    $nids = $query
      ->condition('nid', $context['sandbox']['current_id'], '>')
      ->sort('nid', 'ASC')
      ->range(0, $limit)
      ->execute();

    $path_alias_manager = \Drupal::service('path_alias.manager');
    $queue_factory = \Drupal::service('queue');
    $nodes = $storage->loadMultiple($nids);
    foreach ($nodes as $node) {
      $alias = $path_alias_manager->getAliasByPath('/node/' . $node->id());
      $url = Url::fromUri(
        'internal:' . $alias,
        ['absolute' => TRUE]
      )->toString();
      /** @var \Drupal\Core\Queue\QueueInterface $queue */
      $queue = $queue_factory->get('microwave_node_cron');
      $item = new \stdClass();
      $item->url = $url;
      $queue->createItem($item);
      $context['sandbox']['current_id'] = $node->id();
      $context['results']['result'][] = $context['sandbox']['current_id'];
      $context['sandbox']['progress']++;
      $context['results']['bundle'][$bundle] = $context['sandbox']['progress'];
    }

    if ($context['sandbox']['progress'] !== $context['sandbox']['last_loop'] * $limit) {
      if ($context['sandbox']['progress'] !== $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      return;
    }
    $context['finished'] = 1;
  }

  /**
   * Just add a message when the process is finished.
   */
  public static function batchFinish($success, $results, $operations, $elapsed) {
    $message = t('Finished with an error.');
    $logger = \Drupal::logger('microwave');
    if (!$success || empty($results['result'])) {
      $logger->notice($message);
      return;
    }
    foreach ($results['bundle'] as $bundle => $total) {
      $message = \Drupal::translation()
        ->formatPlural(
          $total,
          $bundle . ': 1 content added to the queue.',
          $bundle . ': @count contents added to the queue.'
        );

      $logger->notice($message);
    }
  }

}
